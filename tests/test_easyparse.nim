import unittest
import options
import sequtils
import sugar
import ../src/easyparse


test "ch":
  let parser = ch('a')
  check parser.debugParse("a") == $('a', "")
  check parser.debugParse("ab") == $('a', "b")
  check parser.debugParse("b") == $(unexpected: "'b'", expected: @["'a'"])

test "letter":
  let parser = letter
  check parser.debugParse("qq") == $('q', "q")
  check parser.debugParse("0") == $(unexpected: "'0'", expected: @["letter"])
  check parser.debugParse("") == $(unexpected: "eof", expected: @["letter"])

test "digit":
  let parser = digit
  check parser.debugParse("1") == $('1', "")
  check parser.debugParse("s") == $(unexpected: "'s'", expected: @["digit"])

test "optional":
  let parser = optional(digit)
  check parser.debugParse("1") == $(some('1'), "")
  check parser.debugParse("") == $(none(char), "")
  check parser.debugParse("a1") == $(none(char), "a1")

test "choice":
  let parser = choice([ch('a'), ch('b')])
  check parser.debugParse("ba") == $('b', "a")
  check parser.debugParse("") == $(unexpected: "eof", expected: @["'a'", "'b'"])

test "fmap":
  proc transform(c: char): string = $c
  let parser = fmap(transform, digit)
  check parser.debugParse("1") == $("1", "")

test "many":
  let parser = many(digit)
  let empty: seq[char] = @[]
  check parser.debugParse("01a") == $(@['0', '1'], "a")
  check parser.debugParse("ab") == $(empty, "ab")
  check parser.debugParse("") == $(empty, "")

test "many . many":
  let parser = many(many(digit))
  check parser.debugParse("012") == $(@[@['0', '1', '2']], "")

test "*>":
  let parser = digit *> letter
  check parser.debugParse("0y1") == $('y', "1")
  check parser.debugParse("17") == $(unexpected: "'7'", expected: @["letter"])
  check parser.debugParse("u") == $(unexpected: "'u'", expected: @["digit"])

test "<*":
  let parser = digit <* letter
  check parser.debugParse("0y1") == $('0', "1")
  check parser.debugParse("17") == $(unexpected: "'7'", expected: @["letter"])
  check parser.debugParse("u") == $(unexpected: "'u'", expected: @["digit"])

test "parseApply":
  func cst(): int = 5
  let p0: Parser[char, int] = parseApply(char, int, cst)
  check p0.debugParse("7") == $(5, "7")

  let p1 = parseApply(char, int, func (c: char): int = int(c), digit)
  check p1.debugParse("7") == $(55, "")
  
  let p2 = parseApply(char, char, func (a: char, b: char): char = b, digit, letter)
  check p2.debugParse("0y") == $('y', "")

test "toSeq":
  let parser = toSeq(digit)
  check parser.debugParse("43") == $(@['4'], "3")

test "oneOf":
  let parser = oneOf("abc")
  check parser.debugParse("a") == $('a', "")
  check parser.debugParse("q") == $(unexpected: "'q'", expected: @["'a'", "'b'", "'c'"])

test "many1":
  let parser = many1(oneOf("ab"))
  check parser.debugParse("aq") == $(@['a'], "q")
  check parser.debugParse("q") == $(unexpected: "'q'", expected: @["'a'", "'b'"])

test "canparse":
  let parser = canparse(digit)
  check parser.debugParse("1") == $(true, "1")
  check parser.debugParse("a") == $(false, "a")

test "<*>":
  let parser = pure[char, char -> seq[char]](func (a: char): seq[char] = @[a]) <*> digit
  check parser.debugParse("1") == $(@['1'], "")

test "eof":
  let parser = ch('a') <* eof[char]()
  check parser.debugParse("a") == $('a', "")
  check parser.debugParse("ab") == $(unexpected: "'b'", expected: @["eof"])

test "failure":
  let parser = ch('a') *> failure[char, Unit]()
  check parser.debugParse("b") == $(unexpected: "'b'", expected: @["'a'"])
  check parser.debugParse("at") == $(unexpected: "'t'", expected: @["failure"])

test "ifthenelse":
  let parser = canparse(ch('a')).ifthenelse(many(letter), many(digit))
  let empty: seq[char] = @[]
  check parser.debugParse("abc") == $(@['a', 'b', 'c'], "")
  check parser.debugParse("654t") == $(@['6', '5', '4'], "t")
  check parser.debugParse("qa") == $(empty, "qa")

test "parseOrRaise":
  let parser = (oneOf "ag") <?> "Letter"
  try:
    discard parser.parseOrRaise("bb")
    doAssert false # Never reach this point
  except ParsingError as e:
    doAssert $e.msg == "Unexpected: 'b' at (1, 0):\nbb\n^\nExpected: 'a' or 'g'\nwhile matching Letter"

test "sepBy":
  let parser = sepBy(ch('a'), ch('.'))
  let empty: seq[char] = @[]
  check parser.debugParse("") == $(empty, "")
  check parser.debugParse(".") == $(empty, ".")
  check parser.debugParse("a.a") == $(@['a', 'a'], "")
  
test "sepBy1":
  let parser = sepBy1(ch('a'), ch('.'))
  check parser.debugParse("") == $(unexpected: "eof", expected: @["'a'"])
  check parser.debugParse("a.a") == $(@['a', 'a'], "")

test "anyChar":
  check anyChar.debugParse("a") == $('a', "")
