# Package
version       = "0.0.0"
author        = "Dario Balboni"
description   = "Easily build parsers using combinators"
license       = "MIT"
srcDir        = "src"


# Dependencies
requires "nim >= 1.4.8"


task test, "Perform all tests for easyparse":
  exec "testament cat /"
