# Easyparse - A parser combinator library for Nim
Easyparse is a parser combinator library written for Nim.
It comes with a very flexible parser class which is suitable for parsing a sequence of elements of any type,
provided that some primitive parsers are available for that type.
Standard parsers for single characters are already included.

Nim generated documentation is available [on GitLab Pages](https://trenta3.gitlab.io/easyparse/).
All available combinators are commented with their action and consulting the documentation is warmly suggested.

## Quickstart
```bash
nimble install https://gitlab.com/trenta3/easyparse
```

Then in your `.nimble` file insert:
```nim
require "https://gitlab.com/trenta3/easyparse@#master"
```

## Examples
### Parsing an integer
Let's see how simple it is to automatically parse a signed integer and convert it to a real integer.
```nim
import sugar
import strutils
import easyparse

let sign: Parser[char, seq[char]] = toSeq(ch('-') <|> ch('+') <|> pure('+'))
let concat: (seq[char], seq[char]) -> seq[char] = `&`
let integer: Parser[char, int] = fmap(parseInt,
    liftA2(concat, sign, many1(digit)),
)

echo $integer.parseOrRaise("-82")
```
