import sugar
import sequtils
import strutils
import options
import macros
import typetraits


proc `==`(S: typedesc, T: typedesc): bool =
  name(S) == name(T)


proc newline*(S: typedesc): S {.inline.} =
  if S == char:
    return '\n'
  else:
    raise newException(ValueError, "No newline for type " & $S)


type
  ParsingError* = object of ValueError
  Position* = object
    line*: int
    column*: int
  
  ParseState*[S] = object
    source*: seq[S]
    pos*: int
    position*: Position

  ParseError*[S] = object
    unexpected*: string
    expected*: seq[string]
    state*: ParseState[S]
    message*: string

  ResultKind* = enum Success, Error
  ParseResult*[S, T] = object
    case kind*: ResultKind
    of Success:
      value*: T
      state*: ParseState[S]
    of Error:
      error*: ParseError[S]

  Parser*[S, T] = ParseState[S] -> ParseResult[S, T]


proc err*[S, T](R: type ParseResult[S, T], x: auto): R {.inline.} =
  ## Set the result to an error.
  R(kind: Error, error: x)


proc ok*[S, T](R: type ParseResult[S, T], state: ParseState[S], value: T): R {.inline.} =
  ## Set the result to a Success
  R(kind: Success, state: state, value: value)


proc isOk*[S, T](res: ParseResult[S, T]): bool {.inline.} =
  case res.kind
  of Success: true
  of Error: false


proc isErr*[S, T](res: ParseResult[S, T]): bool {.inline.} =
  case res.kind
  of Error: true
  of Success: false


type
  EitherKind* = enum Result, Exception
  Either*[R, E] = object
    case kind*: EitherKind
    of Result:
      value*: R
    of Exception:
      error*: E

  ParseOut*[S, T] = object
    result*: T
    rest*: seq[S]


template err*[R, E](A: type Either[R, E], x: auto): A =
  Either[R, E](kind: Exception, error: x)


template ok*[R, E](A: type Either[R, E], x: auto): A =
  Either[R, E](kind: Result, value: x)


proc next*[S](state: ParseState[S]): ParseState[S] {.inline.} =
  ## Advances the parse state by a single character
  if state.pos >= state.source.len:
    # If we are at the end of the stream we return the same state
    return state
  else:
    let
      curs: S = state.source[state.pos]
      news: S = newline(S)
    return ParseState[S](
      source: state.source,
      pos: state.pos + 1,
      position:
        if curs == news:
          Position(line: state.position.line + 1, column: 0)
        else:
          Position(line: state.position.line, column: state.position.column + 1)
    )


proc parse*[S, T](parser: Parser[S, T], source: seq[S]): Either[ParseOut[S, T], ParseError[S]] {.inline.} =
  ## Applies a given `parser` to the `source` and returns an `Either` type.
  let state = ParseState[S](
    source: source,
    pos: 0,
    position: Position(line: 1, column: 0)
  )
  let res = parser state
  case res.kind
  of Success:
    return Either[ParseOut[S, T], ParseError[S]].ok ParseOut[S, T](
      result: res.value,
      rest: res.state.source[res.state.pos ..< res.state.source.len],
    )
  of Error:
    return Either[ParseOut[S, T], ParseError[S]].err res.error


proc debugParse*[T](parser: Parser[char, T], source: string): string {.inline.} =
  ## Used for debugging, returns a string of a tuple
  let src = source.map(c => c)
  let res: Either[ParseOut[char, T], ParseError[char]] = parser.parse(src)
  return case res.kind
         of Result:
           $(res.value.result, res.value.rest.join)
         of Exception:
           $(unexpected: res.error.unexpected, expected: res.error.expected)


proc getLine(source: string, line: int): string =
  ## `getLine` extracts the line `line` from the given source.
  source.split("\n")[line - 1]


proc parseOrRaise*[T](parser: Parser[char, T], source: string): T {.raises: [ParsingError].} =
  ## `parseOrRaise` either returns the parsed element or raises an exception
  let src = source.map(c => c)
  let res = parser.parse(src)
  case res.kind
  of Result:
    return res.value.result
  of Exception:
    let position = res.error.state.position
    raise ParsingError.newException(
      "Unexpected: " & $res.error.unexpected & " at (" & $position.line & ", " & $position.column & "):\n" &
      source.getLine(position.line) & "\n" &
      repeat(" ", position.column) & "^\n" &
      "Expected: " & res.error.expected.mapIt($it).join(" or ") &
      $res.error.message
    )
    

func satisfy*[S](predicate: S -> bool, expected: seq[string] = @[]): Parser[S, S] {.inline.} =
  ## `satisfy` matches a single object that satisfies the given `predicate`.
  return proc (state: ParseState[S]): ParseResult[S, S] =
             if state.pos >= state.source.len:
               # We are past the end of the stream
               return ParseResult[S, S].err ParseError[S](
                 unexpected: "eof",
                 expected: expected,
                 state: state,
                 message: "",
               )
             else:
               let curc = state.source[state.pos]
               if predicate curc:
                 return ParseResult[S, S].ok(next(state), curc)
               else:
                 return ParseResult[S, S].err ParseError[S](
                   unexpected: "'" & $curc & "'",
                   expected: expected,
                   state: state,
                   message: "",
                 )


proc ch*(ch: char): Parser[char, char] {.inline.} =
  ## `ch` matches a single character.
  proc check(c: char): bool = c == ch
  let expected = @["'" & $ch & "'"]
  return satisfy(check, expected)


let letter*: Parser[char, char] = ## `letter` matches ascii letter a-zA-Z.
  satisfy(isAlphaAscii, @["letter"])


let digit*: Parser[char, char] = ## `digit` matches ascii digits 0-9.
  satisfy(isDigit, @["digit"])


func optional*[S, T](parser: Parser[S, T]): Parser[S, Option[T]] {.inline.} =
  ## An `optional` parser returns an `Option` type if `parser` doesn't match.
  return proc (state: ParseState[S]): ParseResult[S, Option[T]] =
             let res = parser state
             if res.isOk:
               return ParseResult[S, Option[T]].ok(res.state, some(res.value))
             else:
               return ParseResult[S, Option[T]].ok(state, none(T))


func choice*[S, T](parsers: openArray[Parser[S, T]]): Parser[S, T] {.inline.} =
  ## `choice` returns the first of `parsers` that matches or an error otherwise.
  let parsers = @parsers
  return proc (state: ParseState[S]): ParseResult[S, T] =
            var expecteds: seq[string] = @[]
            var res: ParseResult[S, T]
            for parser in parsers:
              res = parser state
              if res.isOk:
                return ParseResult[S, T].ok(res.state, res.value)
              else:
                expecteds = expecteds.concat(res.error.expected)
            # If we are here then no parser has matched
            return ParseResult[S, T].err ParseError[S](
              unexpected: res.error.unexpected,
              expected: expecteds,
              state: state,
              message: "",
            )


macro parseApply*(S: untyped, T: untyped, f: untyped, parsers: varargs[untyped]): untyped =
  ## `parseApply` uses the `parsers` one after the others and their parsed results are used as arguments to `f`.
  ## Here `f` must have len(parsers) arguments and return type `T`, while all parsers must be of type `Parser[S, *]`.
  ##
  ## typeof parser[i] == Parser[S, U_i]
  ## typeof f == (U_0, ..., U_n) -> T
  ## Resulting output: Parser[S, T]
  let state = ident("state")
  var retpair = newCall(f)
  var ressymbs: seq[NimNode] = @[]
  for i in 0 ..< parsers.len:
    let resident = ident("res" & $i)
    ressymbs.add ident("res" & $i)
    retpair.add quote do:
      `resident`.value
  # This is the final result after having collected all parsers results
  result = quote do:
      let output: `T` = `retpair`
      return ParseResult[`S`, `T`].ok(`state`, output)
  # Starting from the last parser we build the function recursively
  for i in countdown(parsers.len - 1, 0):
    let resident = ressymbs[i]
    let parser = parsers[i]
    result = quote do:
      let `resident` = `parser` `state`
      if `resident`.isErr:
        return ParseResult[`S`, `T`].err `resident`.error
      else:
        let `state` = `resident`.state
        `result`
  # Then we create the function main body
  result = quote do:
    let parse = proc (`state`: ParseState[`S`]): ParseResult[`S`, `T`] {.closure.} =
      `result`
    parse


func fmap*[S, T, R](fn: T -> R, parser: Parser[S, T]): Parser[S, R] =
  ## `fmap` applies `fn` to the result returned by `parser`
  parseApply(S, R, fn, parser)


func many*[S, T](parser: Parser[S, T]): Parser[S, seq[T]] {.inline.} =
  ## `many` applies `parser` zero or more times until it matches and
  ## collects the obtained result into a sequence.
  ##
  ## To avoid infinite loops the parser stops (and returns an empty sequence)
  ## if the enclosing `parser` returns a result without advancing the state position.
  return proc (state: ParseState[S]): ParseResult[S, seq[T]] =
            var values: seq[T] = @[]
            var res: ParseResult[S, T]
            var newstate: ParseState[S] = state
            while (res = parser newstate; res.isOk):
              # We also want to check whether we have advanced the state.
              if res.state.pos == newstate.pos:
                break
              values.add(res.value)
              newstate = res.state
            return ParseResult[S, seq[T]].ok(newstate, values)
            

func `<|>`*[S, T](p1: Parser[S, T], p2: Parser[S, T]): Parser[S, T] {.inline.} =
  ## `<|>` is an infix operator that acts as `choice` between two parsers
  return choice([p1, p2])


func pure*[S, T](value: T): Parser[S, T] {.inline.} =
  ## `pure` returns a parser that always returns the constant `value`.
  return proc (state: ParseState[S]): ParseResult[S, T] =
             ParseResult[S, T].ok(state, value)


func liftA2*[S, T, R, U](f: (T, R) -> U, p0: Parser[S, T], p1: Parser[S, R]): Parser[S, U] {.inline.} =
  ## `liftA2` matches both parsers and applies `f` to the result.
  parseApply(S, U, f, p0, p1)


func `*>`*[S, T, R](p0: Parser[S, T], p1: Parser[S, R]): Parser[S, R] {.inline.} =
  ## `*>` matches both parsers and returns the result of the second one.
  liftA2(func (t: T, r: R): R = r, p0, p1)


func `<*`*[S, T, R](p0: Parser[S, T], p1: Parser[S, R]): Parser[S, T] {.inline.} =
  ## `<*` matches both parsers and returns the result of the first one.
  liftA2(func (t: T, r: R): T = t, p0, p1)


func oneOf*[S](possibilities: seq[S]): Parser[S, S] {.inline.} =
  ## `oneOf` matches a single element between those passed in the sequence.
  choice(possibilities.mapIt(satisfy((c: S) => c == it, @["'" & $it & "'"])))


func oneOf*(possibilities: string): Parser[char, char] {.inline.} =
  ## `oneOf` for a string divides the string into characters and then uses that standard `oneOf`.
  oneOf(possibilities.mapIt(it))

  
func toSeq*[S, T](parser: Parser[S, T]): Parser[S, seq[T]] {.inline.} =
  ## `toSeq` wraps the returned element from `parser` into a single-item sequence.
  fmap(func (t: T): seq[T] = @[t], parser)


func many1*[S, T](parser: Parser[S, T]): Parser[S, seq[T]] {.inline.} =
  ## `many1` matches `parser` one or more times.
  let cat = func(a: seq[T], b: seq[T]): seq[T] = a & b
  liftA2(cat, toSeq(parser), many(parser))


func `<?>`*[S, T](parser: Parser[S, T], name: string): Parser[S, T] {.inline.} =
  ## `<?>` gives a `name` to the `parser` that augments the error messages given.
  return proc (state: ParseState[S]): ParseResult[S, T] =
             let res = parser state
             if res.isOk:
               return res
             else:
               return ParseResult[S, S].err ParseError[S](
                 unexpected: res.error.unexpected,
                 expected: res.error.expected,
                 state: res.error.state,
                 message: res.error.message & "\nwhile matching " & name,
               )


func canparse*[S, T](parser: Parser[S, T]): Parser[S, bool] {.inline.} =
  ## `canparse` tries to match `parser` and it returns true if a match occurred and false on error.
  ## The state is reset prior to the matching, so that one can do `canparse('a') *> many('a')`.
  return proc (state: ParseState[S]): ParseResult[S, bool] =
             let res = parser state
             return ParseResult[S, bool].ok(state, if res.isOk: true else: false)


func `<*>`*[S, T, R](prec: Parser[S, T -> R], next: Parser[S, T]): Parser[S, R] {.inline.} =
  ## `<*>` matches `prec` and `next` and then applies the result of prec to that of next.
  let applyFunc = func (f: T -> R, el: T): R = f(el)
  liftA2(applyFunc, prec, next)


type Unit* = ##
  ## `Unit` is a void-like type that `eof` returns, because `void` exposes some "edge cases" in the compiler.
  tuple[]
let unit*: Unit = ## `unit` is the only inhabitant of the `Unit` type.
 ()


proc eof*[S](): Parser[S, Unit] {.inline.} =
  ## `eof` matches only the end of the input sequence, and in this case returns `unit`.
  return proc (state: ParseState[S]): ParseResult[S, Unit] =
    if state.pos >= state.source.len:
      return ParseResult[S, Unit].ok(state, unit)
    else:
      return ParseResult[S, Unit].err ParseError[S](
        unexpected: "'" & $state.source[state.pos] & "'",
        expected: @["eof"],
        state: state,
        message: "",
      )


proc failure*[S, R](): Parser[S, R] {.inline.} =
  ## `failure` is a parser that always fails and returns an error
  return proc (state: ParseState[S]): ParseResult[S, R] =
    return ParseResult[S, R].err ParseError[S](
      unexpected: "'" & $state.source[state.pos] & "'",
      expected: @["failure"],
      state: state,
      message: "",
    )


proc ifthenelse*[S, R](condition: Parser[S, bool], iftrue: Parser[S, R], iffalse: Parser[S, R]): Parser[S, R] {.inline.} =
  ## `ifthenelse` uses the `condition` parser, and takes one of the two branch depending on the parsed result.
  return proc (state: ParseState[S]): ParseResult[S, R] =
             let cond: ParseResult[S, bool] = condition state
             if cond.isOk:
               return if cond.value: iftrue cond.state
                      else: iffalse cond.state
             else:
               return ParseResult[S, R].err cond.error


proc sepBy1*[S, R, T](parser: Parser[S, R], separator: Parser[S, T]): Parser[S, seq[R]] =
  ## `sepBy1` matches multiple occurrences of `parser` separated by `separator`.
  ## Parser must match with at least one occurrence.
  let concat: (seq[R], seq[R]) -> seq[R] = `&`
  liftA2(concat, toSeq(parser), many(separator *> parser))

  
proc sepBy*[S, R, T](parser: Parser[S, R], separator: Parser[S, T]): Parser[S, seq[R]] =
  ## `sepBy` matches multiple occurrences of `parser` separated by `separator`.
  ## Separators are discarded, and a sequence of parsers are returned.
  sepBy1(parser, separator) <|> pure[S, seq[R]](@[])


let anyChar*: Parser[char, char] = ## `anyChar` matches any character
  satisfy(func (t: char): bool = true, @["anychar"])
